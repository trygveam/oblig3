module edu.ntnu.idatt2001.trygveam.oblig3 {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001.trygveam.oblig3 to javafx.fxml;
    exports edu.ntnu.idatt2001.trygveam.oblig3;
}